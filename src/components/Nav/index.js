import React from 'react';
import { Link } from 'gatsby';
import { window } from 'browser-monads';
import logo from '../../images/TCM.png';
import './nav.css';

const Nav = () => (
    <nav>
        <div className='nav__items'>
            <a  className='nav__item--left' href='/'><img src={logo} alt='The coding mum logo' className='nav__item--logo'/> </a>
            <Link className={window.location.href.indexOf('home') > 0 ? 'nav__item--link active' : 'nav__item--link'} to='/home'>Home</Link>
            <Link className={window.location.href.indexOf('aboutme') > 0  || window.location.href.indexOf('contact') > 0 ? 'nav__item--link active' : 'nav__item--link'} to='/aboutme'>About Me</Link>
        </div>
    </nav>
)

export default Nav;