import React from "react"
import Layout from "../components/layout"
import Nav from '../components/Nav'
import SEO from "../components/seo"
import Featured from "../components/Featured"

const IndexPage = () => (
  <Layout>
    <SEO title="Home" />
    <Nav />
    <Featured />
  </Layout>
)

export default IndexPage
